import ClientApplicationStepHandles from '@/components/steps/enum/ClientApplicationStepHandles';

export const clientApplicationPathPrefix = 'form';

const clientApplicationRoutes: Record<ClientApplicationStepHandles, string> = {
  [ClientApplicationStepHandles.CONTACT_INFO]: `/${clientApplicationPathPrefix}/${ClientApplicationStepHandles.CONTACT_INFO}`,
  [ClientApplicationStepHandles.MEMBERSHIP]: `/${clientApplicationPathPrefix}/${ClientApplicationStepHandles.MEMBERSHIP}`,
  [ClientApplicationStepHandles.OVERVIEW]: `/${clientApplicationPathPrefix}/${ClientApplicationStepHandles.OVERVIEW}`,
};

export default clientApplicationRoutes;
