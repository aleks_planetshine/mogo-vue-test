import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import { clientApplicationPathPrefix } from '@/router/constants/ClientApplicationRoutes';
import ClientApplicationStepHandles from '@/components/steps/enum/ClientApplicationStepHandles';
import Home from '../views/Home.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: `/${clientApplicationPathPrefix}/:stepHandle`,
    name: 'Application',
    component: () => import('@/components/ClientApplication.vue'),
    props: true,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.name === 'Application') {
    const stepHandle = to.params?.stepHandle;

    if (typeof stepHandle !== 'string'
      || !Object.values(ClientApplicationStepHandles).includes(stepHandle as ClientApplicationStepHandles)) {
      next({ name: 'Home' });

      return;
    }
  }

  next();
});

export default router;
