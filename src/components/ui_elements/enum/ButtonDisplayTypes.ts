enum ButtonDisplayTypes {
  PRIMARY = 'primary',
  DEFAULT = 'default',
}

export default ButtonDisplayTypes;
