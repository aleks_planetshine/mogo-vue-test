enum ButtonTypes {
  BUTTON = 'button',
  SUBMIT = 'submit',
}

export default ButtonTypes;
