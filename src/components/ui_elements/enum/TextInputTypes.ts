enum TextInputTypes {
  TEXT = 'text',
  EMAIL = 'email',
}

export default TextInputTypes;
