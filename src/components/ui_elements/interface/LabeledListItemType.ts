export type LabeledListItemType = [label: string, value: string | number];
