export interface ICodifierValueModel<Codifier extends string> {
  code: Codifier;
  description?: string;
  title: string;
}
