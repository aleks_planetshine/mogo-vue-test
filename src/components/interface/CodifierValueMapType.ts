import { ICodifierValueModel } from '@/components/interface/ICodifierValueModel';

export type CodifierValueMapType<Codifier extends string> = Record<Codifier, ICodifierValueModel<Codifier>>
