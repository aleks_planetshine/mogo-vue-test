import { IClientPrimaryData } from '../steps/interface/IClientPrimaryData';
import { IClientSecondaryData } from '../steps/interface/IClientSecondaryData';

export interface IClientApplicationModel extends IClientPrimaryData, IClientSecondaryData { }
