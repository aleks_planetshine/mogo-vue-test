import PhoneTypeCodes from '@/components/enum/PhoneTypeCodes';

export interface IPhoneNumberModel {
    type: PhoneTypeCodes;
    number: string;
  }
