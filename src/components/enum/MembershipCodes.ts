enum MembershipCodes {
  REGULAR = 'regular',
  PREMIUM = 'premium',
}

export default MembershipCodes;
