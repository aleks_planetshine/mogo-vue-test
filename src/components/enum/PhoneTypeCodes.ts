enum PhoneTypeCodes {
  HOME = 'home',
  WORK = 'work',
  MOBILE = 'mobile',
  MAIN = 'main',
  OTHER = 'other',
}

export default PhoneTypeCodes;
