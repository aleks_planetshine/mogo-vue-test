import { IPhoneNumberModel } from '../../interface/IPhoneNumberModel';

export interface IClientPrimaryData {
  name: string;
  surname: string;
  email: string;
  phones: IPhoneNumberModel[];
}
