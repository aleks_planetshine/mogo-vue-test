import MembershipCodes from '@/components/enum/MembershipCodes';

export interface IClientSecondaryData {
  membershipCode: MembershipCodes;
}
