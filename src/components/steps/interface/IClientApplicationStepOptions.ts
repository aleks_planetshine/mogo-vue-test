import { IClientApplicationStep } from './IClientApplicationStep';

export interface IClientApplicationStepOptions extends IClientApplicationStep {
    isActive: boolean;
    isCompleted: boolean;
}
