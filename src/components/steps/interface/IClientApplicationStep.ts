import ClientApplicationStepHandles from '@/components/steps/enum/ClientApplicationStepHandles';

export interface IClientApplicationStep {
    handle: ClientApplicationStepHandles
    order: number,
    title: string,
}
