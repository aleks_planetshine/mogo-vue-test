enum ClientApplicationStepHandles {
  CONTACT_INFO = 'contact-info',
  MEMBERSHIP = 'membership',
  OVERVIEW = 'overview',
}

export default ClientApplicationStepHandles;
