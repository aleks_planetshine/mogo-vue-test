import PhoneTypeCodes from '@/components/enum/PhoneTypeCodes';
import { ICodifierValueModel } from '@/components/interface/ICodifierValueModel';

const phoneCodifierValues: ICodifierValueModel<PhoneTypeCodes>[] = [
  { code: PhoneTypeCodes.HOME, title: 'Home' },
  { code: PhoneTypeCodes.WORK, title: 'Work' },
  { code: PhoneTypeCodes.MOBILE, title: 'Mobile' },
  { code: PhoneTypeCodes.MAIN, title: 'Main' },
  { code: PhoneTypeCodes.OTHER, title: 'Other' },
];

export default phoneCodifierValues;
