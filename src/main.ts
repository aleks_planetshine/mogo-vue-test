import { createApp } from 'vue';
import Antd from 'ant-design-vue';
import App from './App.vue';
import router from './router';
import { store } from './store';
import 'reset-css/sass/_reset.scss';
import 'ant-design-vue/dist/antd.css';
import './assets/styles/fonts.css';
import './assets/styles/main.scss';

createApp(App)
  .use(Antd)
  .use(store)
  .use(router)
  .mount('#app');
