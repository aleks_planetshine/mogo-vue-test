import { CommitOptions, DispatchOptions, Store as VuexStore } from 'vuex';
import { IState } from '@/store/interface/IState';
import { IMutations } from '@/store/interface/IMutations';
import { IActions } from '@/store/interface/IActions';
import { IGetters } from '@/store/interface/IGetters';

export type StoreType = Omit<
  VuexStore<IState>,
  'getters' | 'commit' | 'dispatch'
  > & {
  commit<K extends keyof IMutations, P extends Parameters<IMutations[K]>[1]>(
    key: K,
    payload: P,
    options?: CommitOptions
  ): ReturnType<IMutations[K]>
} & {
  dispatch<K extends keyof IActions>(
    key: K,
    payload?: Parameters<IActions[K]>[1],
    options?: DispatchOptions
  ): ReturnType<IActions[K]>
} & {
  getters: {
    [K in keyof IGetters]: ReturnType<IGetters[K]>
  }
}
