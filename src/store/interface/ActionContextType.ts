import { ActionContext } from 'vuex';
import { IMutations } from './IMutations';
import { IState } from './IState';

export type ActionContextType = Omit<ActionContext<IState, IState>, 'commit'> & {
  commit<K extends keyof IMutations>(
    key: K,
    payload: Parameters<IMutations[K]>[1]
    ): ReturnType<IMutations[K]>
}
