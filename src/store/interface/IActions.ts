import ActionMethods from '../enum/ActionMethods';
import { ActionContextType } from './ActionContextType';

export interface IActions {
  [ActionMethods.CREATE_CLIENT_APPLICATION](context: ActionContextType): void
}
