import { IClientApplicationModel } from '@/components/interface/IClientApplicationModel';

export interface IState {
  clientApplication: IClientApplicationModel;
  isModalOpen: boolean;
  isLoading: boolean;
}
