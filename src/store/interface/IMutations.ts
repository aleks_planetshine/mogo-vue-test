import { IPhoneNumberModel } from '@/components/interface/IPhoneNumberModel';
import MembershipCodes from '@/components/enum/MembershipCodes';
import PhoneTypeCodes from '@/components/enum/PhoneTypeCodes';
import MutationMethods from '../enum/MutationMethods';
import { IState } from './IState';

export interface IMutations {
  [MutationMethods.SET_NAME](state: IState, name: string): void,
  [MutationMethods.SET_SURNAME](state: IState, surname: string): void,
  [MutationMethods.SET_EMAIL](state: IState, email: string): void,
  [MutationMethods.ADD_PHONE](state: IState, type: PhoneTypeCodes): void,
  [MutationMethods.SET_PHONE](state: IState, updatedPhone: IPhoneNumberModel): void,
  [MutationMethods.SET_MEMBERSHIP_CODE](state: IState, membershipCode: MembershipCodes): void,
  [MutationMethods.SET_IS_MODAL_OPEN](state: IState, isModalOpen: boolean): void,
  [MutationMethods.SET_IS_LOADING](state: IState, isLoading: boolean): void,
}
