import { IState } from '@/store/interface/IState';
import { IClientApplicationStep } from '@/components/steps/interface/IClientApplicationStep';
import ClientApplicationStepHandles from '@/components/steps/enum/ClientApplicationStepHandles';
import { IClientApplicationStepOptions } from '@/components/steps/interface/IClientApplicationStepOptions';
import { CodifierValueMapType } from '@/components/interface/CodifierValueMapType';
import MembershipCodes from '@/components/enum/MembershipCodes';
import PhoneTypeCodes from '@/components/enum/PhoneTypeCodes';

export interface IGetters {
  getClientApplicationStepByHandle(state: IState): (handle: ClientApplicationStepHandles) => IClientApplicationStep | undefined;
  getClientApplicationStepsConfiguration(state: IState): (handle: ClientApplicationStepHandles) => IClientApplicationStepOptions[];
  phoneCodifierMap(state: IState): CodifierValueMapType<PhoneTypeCodes>;
  membershipCodifierMap(state: IState): CodifierValueMapType<MembershipCodes>;
}
