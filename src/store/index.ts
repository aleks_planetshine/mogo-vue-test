import { createLogger, createStore } from 'vuex';
import { IState } from '@/store/interface/IState';
import { StoreType } from '@/store/interface/StoreType';
import state from './constants/state';
import actions from './constants/actions';
import mutations from './constants/mutations';
import getters from './constants/getters';

export const store = createStore<IState>({
  plugins: process.env.NODE_ENV === 'development' ? [createLogger()] : [],
  state,
  mutations,
  actions,
  getters,
});

export function useStore(): StoreType {
  return store;
}
