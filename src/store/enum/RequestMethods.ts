enum RequestMethods {
  GET = 'GET',
  POST = 'POST',
}

export default RequestMethods;
