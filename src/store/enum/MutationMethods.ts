enum MutationMethods {
  SET_NAME = 'setName',
  SET_SURNAME = 'setSurname',
  SET_EMAIL = 'setEmail',
  ADD_PHONE = 'addPhone',
  SET_PHONE = 'setPhone',
  SET_MEMBERSHIP_CODE = 'setMembershipCode',
  SET_IS_MODAL_OPEN = 'setIsModalOpen',
  SET_IS_LOADING = 'setIsLoading',
}

export default MutationMethods;
