import { IActions } from '@/store/interface/IActions';
import { IState } from '@/store/interface/IState';
import { ActionTree } from 'vuex';
import ActionMethods from '@/store/enum/ActionMethods';
import MutationMethods from '@/store/enum/MutationMethods';
import getRequestOptions from '@/store/helpers/RequestHelper';
import RequestMethods from '@/store/enum/RequestMethods';
import handleResponseErrors from '@/store/helpers/RequestErrorHelper';

const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

const actions: ActionTree<IState, IState> & IActions = {
  async [ActionMethods.CREATE_CLIENT_APPLICATION]({ commit, state }) {
    if (state.isLoading) {
      return;
    }

    commit(MutationMethods.SET_IS_LOADING, true);

    await fetch(
      'https://httpreq.com/round-queen-3zanbf45/record',
      getRequestOptions(RequestMethods.POST, state.clientApplication),
    )
      .then(handleResponseErrors)
      .then((response) => console.log('Successfully created', response))
      .catch((error) => console.log('An error occurred', error));

    // simulate waiting
    await sleep(1500);

    commit(MutationMethods.SET_IS_LOADING, false);
  },
};

export default actions;
