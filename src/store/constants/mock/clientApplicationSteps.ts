import { IClientApplicationStep } from '@/components/steps/interface/IClientApplicationStep';
import ClientApplicationStepHandles
  from '../../../components/steps/enum/ClientApplicationStepHandles';

const clientApplicationSteps: IClientApplicationStep[] = [
  {
    handle: ClientApplicationStepHandles.CONTACT_INFO,
    order: 1,
    title: 'Personal info',
  },
  {
    handle: ClientApplicationStepHandles.MEMBERSHIP,
    order: 2,
    title: 'Membership',
  },
  {
    handle: ClientApplicationStepHandles.OVERVIEW,
    order: 3,
    title: 'Overview',
  },
];

export default clientApplicationSteps;
