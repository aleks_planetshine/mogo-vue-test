import { GetterTree } from 'vuex';
import { IState } from '@/store/interface/IState';
import { IGetters } from '@/store/interface/IGetters';
import clientApplicationSteps from '@/store/constants/mock/clientApplicationSteps';
import ClientApplicationStepHandles from '@/components/steps/enum/ClientApplicationStepHandles';
import getClientApplicationStepByHandle from '@/store/helpers/ClientApplicationStepHelper';
import MembershipCodes from '@/components/enum/MembershipCodes';
import membershipCodifierValues from '@/components/constants/membershipCodifierValues';
import PhoneTypeCodes from '@/components/enum/PhoneTypeCodes';
import phoneCodifierValues from '@/components/constants/phoneCodifierValues';
import getCodifierValueMap from '@/store/helpers/CodifierValueMapHelper';

const getters: GetterTree<IState, IState> & IGetters = {
  getClientApplicationStepByHandle:
    () => (handle: ClientApplicationStepHandles) => getClientApplicationStepByHandle(clientApplicationSteps, handle),
  getClientApplicationStepsConfiguration: () => (handle: ClientApplicationStepHandles) => {
    const activeStep = getClientApplicationStepByHandle(clientApplicationSteps, handle);
    if (!activeStep) {
      return clientApplicationSteps.map((step) => (
        {
          ...step,
          isActive: false,
          isCompleted: false,
        }
      ));
    }

    const { handle: activeStepHandle, order: activeStepOrder } = activeStep;

    return clientApplicationSteps.map((step) => ({
      ...step,
      isActive: step.handle === activeStepHandle,
      isCompleted: step.order < activeStepOrder,
    }));
  },
  phoneCodifierMap: () => getCodifierValueMap<PhoneTypeCodes>(phoneCodifierValues),
  membershipCodifierMap: () => getCodifierValueMap<MembershipCodes>(membershipCodifierValues),
};

export default getters;
