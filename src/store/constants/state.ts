import MembershipCodes from '@/components/enum/MembershipCodes';
import PhoneTypeCodes from '@/components/enum/PhoneTypeCodes';
import { IState } from '../interface/IState';

const state: IState = {
  clientApplication: {
    name: '',
    surname: '',
    email: '',
    phones: [
      {
        number: '',
        type: PhoneTypeCodes.MAIN,
      },
    ],
    membershipCode: MembershipCodes.REGULAR,
  },
  isModalOpen: false,
  isLoading: false,
};

export default state;
