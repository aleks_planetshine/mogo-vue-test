import { MutationTree } from 'vuex';
import getClientApplicationPhoneIndexByCode from '@/store/helpers/ClientApplicationPhoneHelper';
import MutationMethods from '../enum/MutationMethods';
import { IMutations } from '../interface/IMutations';
import { IState } from '../interface/IState';

const mutations: MutationTree<IState> & IMutations = {
  [MutationMethods.SET_NAME](state, name) {
    state.clientApplication.name = name;
  },
  [MutationMethods.SET_SURNAME](state, surname) {
    state.clientApplication.surname = surname;
  },
  [MutationMethods.SET_EMAIL](state, email) {
    state.clientApplication.email = email;
  },
  [MutationMethods.ADD_PHONE](state, type) {
    if (getClientApplicationPhoneIndexByCode(state.clientApplication.phones, type) === -1) {
      state.clientApplication.phones.push({
        type,
        number: '',
      });
    }
  },
  [MutationMethods.SET_PHONE](state, updatedPhone) {
    const { phones } = state.clientApplication;

    const phoneIndex = getClientApplicationPhoneIndexByCode(phones, updatedPhone.type);
    if (phoneIndex !== -1) {
      phones[phoneIndex] = { ...phones[phoneIndex], ...updatedPhone };
    }
  },
  [MutationMethods.SET_MEMBERSHIP_CODE](state, membershipCode) {
    state.clientApplication.membershipCode = membershipCode;
  },
  [MutationMethods.SET_IS_MODAL_OPEN](state, isModalOpen) {
    state.isModalOpen = isModalOpen;
  },
  [MutationMethods.SET_IS_LOADING](state, isLoading) {
    state.isLoading = isLoading;
  },
};

export default mutations;
