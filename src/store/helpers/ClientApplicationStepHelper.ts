import ClientApplicationStepHandles from '@/components/steps/enum/ClientApplicationStepHandles';
import { IClientApplicationStep } from '@/components/steps/interface/IClientApplicationStep';

const getClientApplicationStepByHandle = (
  clientApplicationSteps: IClientApplicationStep[], handle: ClientApplicationStepHandles,
): IClientApplicationStep | undefined => clientApplicationSteps.find((step) => step.handle === handle);

export default getClientApplicationStepByHandle;
