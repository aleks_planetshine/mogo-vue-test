import RequestMethods from '@/store/enum/RequestMethods';

const getRequestOptions = <PayloadType>(method: RequestMethods, payload: PayloadType): RequestInit => {
  const options: RequestInit = {
    method: method as string,
    headers: {
      'Content-Type': 'application/json',
    },
    mode: 'no-cors',
  };

  if (method === RequestMethods.POST && payload) {
    options.body = JSON.stringify(payload);
  }

  return options;
};

export default getRequestOptions;
