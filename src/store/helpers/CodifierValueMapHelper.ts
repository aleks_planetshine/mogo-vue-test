import { ICodifierValueModel } from '@/components/interface/ICodifierValueModel';
import { CodifierValueMapType } from '@/components/interface/CodifierValueMapType';

const getCodifierValueMap = <Codifier extends string>(
  codifierValues: ICodifierValueModel<Codifier>[],
): CodifierValueMapType<Codifier> => {
  const map = {} as CodifierValueMapType<Codifier>;
  codifierValues.forEach((codifierValue) => {
    map[codifierValue.code] = codifierValue;
  });

  return map;
};

export default getCodifierValueMap;
