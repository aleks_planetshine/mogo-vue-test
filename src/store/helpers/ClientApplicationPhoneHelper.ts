import { IPhoneNumberModel } from '@/components/interface/IPhoneNumberModel';
import PhoneTypeCodes from '@/components/enum/PhoneTypeCodes';

const getClientApplicationPhoneIndexByCode = (
  phones: IPhoneNumberModel[], code: PhoneTypeCodes,
): number => phones.findIndex((phone) => phone.type === code);

export default getClientApplicationPhoneIndexByCode;
